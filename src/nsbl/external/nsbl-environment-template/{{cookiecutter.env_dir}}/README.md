# Auto-generated Ansible environment

This Ansible playbook environment was automatically created by [nsbl](https://github.com/nsbl/nsbl) from the [nsbl-environment-template](https://gitlab.com/nsbl/nsbl-environment-template) cookiecutter template.

## Licensing

The license for the template and its supporting files is [The Parity Public License, v3.0.0](https://licensezero.com/licenses/parity).

For the copyright information and licenses of included Ansible roles, task-lists and modules and plugins please check their source code and/or metadata.

For copyright information of auto-generated playbooks please contact whoever wrote the source configuration for this target and distributed this environment.  
